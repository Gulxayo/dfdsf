import React from 'react';
import '../../App.css';
import {Row, Col, Container } from 'reactstrap'
import {
  Card, Button, CardImg, CardTitle, CardText, CardDeck,
  CardSubtitle, CardBody
} from 'reactstrap';
function Blogs() {
  return (
    <>
    <div className="bg-light">
    <h2 className="p-3 text-center">Eng so'ngi e'lonlar !!!</h2>
    <div className="BorderPa">
    <Row xs="1" sm="2" md="4">
      <Col >
      <CardDeck>
      <Card >
        <CardImg top width="100%" className="blogImg" src="/images/blog-1.jpg" alt="Card image cap" />
        </Card>
    </CardDeck>
      </Col >
      <Col >
       <h4>Qachon bo'ladi-ya shu Meeting (Uchrashuv)? </h4>
       <p>Ko'p savollar kelib tushdi qachon va yana qachon "Ismoil Safarov bilan Work Field (http://t.me/Work_Field) jamoasi uchrashuv tayyorlaydi". Uncha 🏃‍♂shoshqoloqlik qilmang. Sizlarni uzoq kuttirmagan holda 13-dekabr yakshanba kuni soat 19:00da ZOOM platformasi orqali bo'lib o'tadi.
        Ha aytgancha hali ham ZOOM ilovasini yuklab olmagan bo'lsangiz hoziroq yuklab oling. Ertagalik uchrashuvdan mosivo bo'lib qolmang❗️</p>
        
      </Col>
      <Col >
      <CardDeck>
      <Card >
        <CardImg top width="100%" className="blogImg" src="/images/blog-2.jpg" alt="Card image cap" />
        </Card>
    </CardDeck>
      </Col >
      <Col >
       <h4>SMM bo'yicha ochiq dars! </h4>
       <p>25-noyabr soat 16:00 da Work Field jamoasidan ochiq darsni kutib oling. Siz ochiq darsda sizga kerakli bo'lgan savollarga javob olib qo'shimcha ma'lumotlarga ham ega bo'lasiz.</p>
        <p>🤙Ochiq dars mavzusi: SMMning siz bilgan va bilmagan ma'lumotlar va ajoyib savollarga javob</p>
        <p>🗣Speaker: Farrux Abrorkulov</p>
      </Col>
    </Row>
     
    <Row xs="1" sm="2" md="4">
      <Col >
       <h4>Qachon bo'ladi-ya shu Meeting (Uchrashuv)? </h4>
       <p>Ko'p savollar kelib tushdi qachon va yana qachon "Ismoil Safarov bilan Work Field (http://t.me/Work_Field) jamoasi uchrashuv tayyorlaydi". Uncha 🏃‍♂shoshqoloqlik qilmang. Sizlarni uzoq kuttirmagan holda 13-dekabr yakshanba kuni soat 19:00da ZOOM platformasi orqali bo'lib o'tadi.
        Ha aytgancha hali ham ZOOM ilovasini yuklab olmagan bo'lsangiz hoziroq yuklab oling. Ertagalik uchrashuvdan mosivo bo'lib qolmang❗️</p>
        
      </Col>
      <Col >
      <CardDeck>
      <Card >
        <CardImg top width="100%" className="blogImg" src="/images/blog-5.jpg" alt="Card image cap" />
        </Card>
    </CardDeck>
      </Col >
      <Col >
       <h4>SMM bo'yicha ochiq dars! </h4>
       <p>25-noyabr soat 16:00 da Work Field jamoasidan ochiq darsni kutib oling. Siz ochiq darsda sizga kerakli bo'lgan savollarga javob olib qo'shimcha ma'lumotlarga ham ega bo'lasiz.</p>
        <p>🤙Ochiq dars mavzusi: SMMning siz bilgan va bilmagan ma'lumotlar va ajoyib savollarga javob</p>
        <p>🗣Speaker: Farrux Abrorkulov</p>
      </Col>
      <Col >
      <CardDeck>
      <Card >
        <CardImg top width="100%" className="blogImg" src="/images/blog-4.jpg" alt="Card image cap" />
        </Card>
    </CardDeck>
      </Col >
    </Row>
    
    </div>
    
    <Container mt="5"  className="paragraf">
      <p >
      Bugungi kunda freelancer sifatida ish topish va daromad ko'rish uchun ko'pgina platformalar o'z eshiklarini ijodkorlarga baralla ochgan. Lekin bizningcha ushbu usul hozirga kelib o'zbek IT bozorida sal oqsayotgandek, yani freelancerlar ko'p lekin o'z ishining mutaxassislari juda kam. 
      </p>
      <p>
      Bugun sizga odatiy usuldan umuman farq qiladigan va sinalgan, natija bera oladigan usulni  taklif qilmoqchimiz. Albatta o'zingizda sinab ko'ring, natija berishiga kafolat beramiz. Faqat qunt bilan bajarsangiz bas!
      </p>
    </Container>

    <div className="Border2">
      
    
    <Row xs="1" sm="2" md="2">
      <Col className="Border2">
      <CardDeck>
      <Card >
        <CardImg top width="100%" className="blogImg" src="/images/blogm-1.jpg" alt="Card image cap" />
        </Card>
    </CardDeck>
      </Col >
      <Col className="Border2">
      <CardDeck>
      <Card >
        <CardImg top width="100%" className="blogImg" src="/images/blogm-2.jpg" alt="Card image cap" />
        </Card>
    </CardDeck>
      </Col>
    </Row>


    <Row xs="1" sm="3" md="3">
      <Col className="Border2">
      <CardDeck>
      <Card >
        <CardImg top width="100%" className="blogImg" src="/images/blogm-3.jpg" alt="Card image cap" />
        </Card>
    </CardDeck>
      </Col >
      <Col className="Border2">
      <CardDeck>
      <Card >
        <CardImg top width="100%" className="blogImg" src="/images/blogm-4.jpg" alt="Card image cap" />
        </Card>
    </CardDeck>
      </Col >
      <Col className="Border2" >
      <CardDeck>
      <Card >
        <CardImg top width="100%" className="blogImg" src="/images/blogm-5.jpg" alt="Card image cap" />
        </Card>
    </CardDeck>
      </Col >
    </Row>

     
    <Row xs="1" sm="3" md="3">
      <Col className="Border2">
      <CardDeck>
      <Card >
        <CardImg top width="100%" className="blogImg" src="/images/blogm-6.jpg" alt="Card image cap" />
        </Card>
    </CardDeck>
      </Col >
      <Col className="Border2">
      <CardDeck>
      <Card >
        <CardImg top width="100%" className="blogImg" src="/images/blogm-7.jpg" alt="Card image cap" />
        </Card>
    </CardDeck>
      </Col >
      <Col className="Border2">
      <CardDeck>
      <Card >
        <CardImg top width="100%" className="blogImg" src="/images/blogm-8.jpg" alt="Card image cap" />
        </Card>
    </CardDeck>
      </Col >
    </Row>

    </div>

     </div>
    </>
  );
}

export default Blogs;